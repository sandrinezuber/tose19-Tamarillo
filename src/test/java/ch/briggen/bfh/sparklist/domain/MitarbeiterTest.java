package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MitarbeiterTest {	
	

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Mitarbeiter i = new Mitarbeiter();
		assertThat("id",i.getMA_ID(),is(equalTo(0l)));
		assertThat("name",i.getMA_Name(),is(equalTo(null)));
		assertThat("vorname",i.getMA_Vorname(),is(equalTo(null)));
		assertThat("geschlecht",i.getMA_Geschlecht(),is(equalTo(null)));
		assertThat("strasse",i.getMA_Strasse(),is(equalTo(null)));
		assertThat("hausnummer",i.getMA_Hausnummer(),is(equalTo(null)));
		assertThat("plz",i.getMA_PLZ(),is(equalTo(null)));
		assertThat("ort",i.getMA_Ort(),is(equalTo(null)));
		assertThat("land",i.getMA_Land(),is(equalTo(null)));
		assertThat("geburtsdatum",i.getMA_Geburtsdatum(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,Leuver,Lars,Männlich,Blumenstrasse,12a,3012,Bern,CH,1947-04-01"})
	void testContructorAssignsAllFields(long id, String name, String vorname, String geschlecht, String strasse, String hausnummer, Integer plz, String ort, String land, String geburtsdatum) {
		Date date = convertDate(geburtsdatum);
		//Date must be parseable
		assertTrue(date != null);
		
		Mitarbeiter i = new Mitarbeiter(id, name, vorname, geschlecht, strasse, hausnummer, plz, ort, land, date);
		assertThat("id",i.getMA_ID(),equalTo(id));
		assertThat("name",i.getMA_Name(),equalTo(name));
		assertThat("vorname", i.getMA_Vorname(),equalTo(vorname));
		assertThat("geschlecht", i.getMA_Geschlecht(),equalTo(geschlecht));		
		assertThat("strasse", i.getMA_Strasse(),equalTo(strasse));
		assertThat("hausnummer", i.getMA_Hausnummer(),equalTo(hausnummer));
		assertThat("plz", i.getMA_PLZ(),equalTo(plz));
		assertThat("ort", i.getMA_Ort(),equalTo(ort));
		assertThat("land", i.getMA_Land(),equalTo(land));
		assertThat("geburtsdatum",i.getMA_Geburtsdatum(),equalTo(date));
	}
	
	@ParameterizedTest
	@CsvSource({"1,Leuver,Lars,Männlich,Blumenstrasse,12a,3012,Bern,CH,1947-04-01"})
	void testSetters(long id, String name, String vorname, String geschlecht, String strasse, String hausnummer, Integer plz, String ort, String land, String geburtsdatum) {
		Date date = convertDate(geburtsdatum);
		//Date must be parseable
		assertTrue(date != null);
		
		Mitarbeiter i = new Mitarbeiter();
		i.setMA_ID(id);
		i.setMA_Name(name);
		i.setMA_Vorname(vorname);
		i.setMA_Geschlecht(geschlecht);
		i.setMA_Strasse(strasse);
		i.setMA_Hausnummer(hausnummer);
		i.setMA_PLZ(plz);
		i.setMA_Ort(ort);
		i.setMA_Land(land);
		i.setMA_Geburtsdatum(date);
		assertThat("id",i.getMA_ID(),equalTo(id));
		assertThat("name",i.getMA_Name(),equalTo(name));
		assertThat("vorname",i.getMA_Vorname(),equalTo(vorname));
		assertThat("geschlecht",i.getMA_Geschlecht(),equalTo(geschlecht));		
		assertThat("strasse",i.getMA_Strasse(),equalTo(strasse));
		assertThat("hausnummer",i.getMA_Hausnummer(),equalTo(hausnummer));
		assertThat("plz",i.getMA_PLZ(),equalTo(plz));
		assertThat("ort",i.getMA_Ort(),equalTo(ort));
		assertThat("land",i.getMA_Land(),equalTo(land));
		assertThat("geburtsdatum",i.getMA_Geburtsdatum(),equalTo(date));
	}
	
	@Test
	void testToString() {
		Mitarbeiter i = new Mitarbeiter();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
	
	private Date convertDate(String date) {
		String dateFormatPattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatPattern);
		try {
			return simpleDateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
