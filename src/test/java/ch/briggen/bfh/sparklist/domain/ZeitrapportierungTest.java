package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ZeitrapportierungTest {


	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Zeitrapportierung i = new Zeitrapportierung();
		assertThat("id",i.getId(),is(equalTo(0l)));
		assertThat("tag",i.getTag(),is(equalTo(null)));
		assertThat("stunden",i.getStunden(),is(equalTo(0.0F)));
		assertThat("ma_id",i.getMa_id(),is(equalTo(0)));
		assertThat("ro_id",i.getRo_id(),is(equalTo(0)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,2019-01-01,1.1,1,1","2,2019-02-02,2.2,2,2","3,2019-03-03,3.3,3,3"})
	void testContructorAssignsAllFields(long id, String tag, float stunden, int ma_id, int ro_id) {
		Date date = convertDate(tag);
		//Date must be parseable
		assertTrue(date != null);
		
		Zeitrapportierung i = new Zeitrapportierung(id, date, stunden, ma_id, ro_id);
		assertThat("id",i.getId(),equalTo(id));
		assertThat("tag", i.getTag(),equalTo(date));
		assertThat("stunden", i.getStunden(),equalTo(stunden));
		assertThat("ma_id",i.getMa_id(),equalTo(ma_id));
		assertThat("ro_id",i.getRo_id(),equalTo(ro_id));
	}
	
	@ParameterizedTest
	@CsvSource({"1,2019-01-01,1.1,1,1","2,2019-02-02,2.2,2,2","3,2019-03-03,3.3,3,3"})
	void testSetters(long id, String tag, float stunden, int ma_id, int ro_id) {
		Date date = convertDate(tag);
		//Date must be parseable
		assertTrue(date != null);
		
		Zeitrapportierung i = new Zeitrapportierung();
		i.setId(id);
		i.setTag(date);
		i.setStunden(stunden);
		i.setMa_id(ma_id);
		i.setRo_id(ro_id);
		assertThat("id",i.getId(),equalTo(id));
		assertThat("tag",i.getTag(),equalTo(date));
		assertThat("stunden",i.getStunden(),equalTo(stunden));
		assertThat("ma_id",i.getMa_id(),equalTo(ma_id));
		assertThat("ro_id",i.getRo_id(),equalTo(ro_id));
	}
	
	@Test
	void testToString() {
		Zeitrapportierung i = new Zeitrapportierung();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
	
	private Date convertDate(String date) {
		String dateFormatPattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatPattern);
		try {
			return simpleDateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
