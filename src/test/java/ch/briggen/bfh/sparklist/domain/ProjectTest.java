package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ProjectTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Project i = new Project();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testContructorAssignsAllFields(long id,String name, int k_id) {
		Project i = new Project(id, name, k_id);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
		assertThat("K_ID", i.getK_Id(),equalTo(k_id));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testSetters(long id,String name,int k_id) {
		Project i = new Project();
		i.setId(id);
		i.setName(name);
		i.setK_Id(k_id);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
		assertThat("K_ID",i.getK_Id(),equalTo(k_id));
	}
	
	@Test
	void testToString() {
		Project i = new Project();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
