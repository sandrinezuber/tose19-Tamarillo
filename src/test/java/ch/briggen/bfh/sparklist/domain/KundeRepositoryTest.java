package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.KundeDBTestHelper.*;


import java.util.Collection;
import java.util.NoSuchElementException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class KundeRepositoryTest {

	KundeRepository repo = null;

	
	private static void populateRepo(KundeRepository r) {
		for(int i = 0; i <10; ++i) {
			Kunde dummy = new Kunde(0,"Fake Test Kunde" + i);
			r.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new KundeRepository();
	}

	@Test
	void testEmptyDB() {
		assertThat("New DB must be empty",repo.getAll().isEmpty(),is(true));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		assertThat("Freshly populated DB must hold 10 entries",repo.getAll().size(),is(10));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One","2,Two","3,Three"})
	void testInsertKundes(long id,String name)
	{
		populateRepo(repo);
		
		Kunde i = new Kunde(id,name);
		long dbId = repo.insert(i);
		Kunde fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,Eins","2,Two,Zwei","3,Three,Drei"})
	void testUpdateKundes(long id,String name,String newName)
	{
		populateRepo(repo);
		
		Kunde i = new Kunde(id,name);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setName(newName);
		repo.save(i);
		
		Kunde fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(newName) );
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One","2,Two","3,Three"})
	void testDeleteKundes(long id,String name)
	{
		populateRepo(repo);
		
		Kunde i = new Kunde(id,name);
		long dbId = repo.insert(i);
		Kunde fromDB = repo.getById(dbId);
		assertThat("Kunde was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{repo.getById(dbId);},"Kunde should have been deleted");
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Kunde i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all Kundes",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One","2,Two","3,Three"})
	void testGetByOneName(long id,String name)
	{
		populateRepo(repo);
		
		Kunde i = new Kunde(id,name);
		repo.insert(i);
		Collection<Kunde> fromDB = repo.getByName(name);
		assertThat("Exactly one Kunde was returned", fromDB.size(),is(1));
		
		Kunde elementFromDB = fromDB.iterator().next();
		
		assertThat("name = name", elementFromDB.getName(),is(name) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One","2,Two","3,Three"})
	void testGetManyKundesByName(int count,String name)
	{
		populateRepo(repo);
		
		
		for(int n = 0; n < count; ++n) {
			Kunde i = new Kunde(0,name);
			repo.insert(i);
		}
		
		Collection<Kunde> fromDB = repo.getByName(name);
		assertThat("Exactly one Kunde was returned", fromDB.size(),is(count));
		
		for(	Kunde elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
		}
	}
	
	@Test
	void testGetNoKundesByName()
	{
		populateRepo(repo);
		
		Collection<Kunde> fromDB = repo.getByName("NotExistingKunde");
		assertThat("Exactly one Kunde was returned", fromDB.size(),is(0));
		
	}
}