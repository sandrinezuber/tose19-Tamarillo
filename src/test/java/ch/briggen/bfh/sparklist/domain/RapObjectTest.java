package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RapObjectTest {


	@Test
	void testEmptyContructorYieldsEmptyObject() {
		RapObject i = new RapObject();
		assertThat("ro_id",i.getId(),is(equalTo(0l)));
		assertThat("ro_name",i.getName(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testContructorAssignsAllFields(long ro_id,String ro_name, int p_id) {
		RapObject i = new RapObject(ro_id, ro_name, p_id);
		assertThat("ro_id",i.getId(),equalTo(ro_id));
		assertThat("ro_name",i.getName(),equalTo(ro_name));
		assertThat("p_id", i.getProjectID(),equalTo(p_id));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,-1","2,Two,0","3,Three,1"})
	void testSetters(long ro_id,String ro_name,int p_id) {
		RapObject i = new RapObject();
		i.setId(ro_id);
		i.setName(ro_name);
		i.setProjectID(p_id);;
		assertThat("ro_id",i.getId(),equalTo(ro_id));
		assertThat("ro_name",i.getName(),equalTo(ro_name));
		assertThat("p_id",i.getProjectID(),equalTo(p_id));
	}
	
	@Test
	void testToString() {
		RapObject i = new RapObject();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
