package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class KundeTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Kunde i = new Kunde();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One","2,Two","3,Three"})
	void testContructorAssignsAllFields(long id,String name) {
		Kunde i = new Kunde(id, name);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One","2,Two","3,Three"})
	void testSetters(long id,String name) {
		Kunde i = new Kunde();
		i.setId(id);
		i.setName(name);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name",i.getName(),equalTo(name));
	}
	
	@Test
	void testToString() {
		Kunde i = new Kunde();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
