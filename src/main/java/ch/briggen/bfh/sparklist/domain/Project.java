package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag im Projekt mit Name und eindeutiger ID
 * @author Dominique Gilgen
 *
 */
public class Project {
	
	private long id;
	private String name;
	private int k_id;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Project()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige ID
	 * @param name Name des Projektes
	 * @param k_id ID des Kunden
	 */
	public Project(long id, String name, int k_id)
	{
		this.id = id;
		this.name = name;
		this.k_id = k_id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getK_Id() {
		return k_id;
	}
	
	public void setK_Id(int k_id) {
		this.k_id = k_id;
	}
	
	@Override
	public String toString() {
		return String.format("Project:{id: %d; name: %s, k_id: %d;}", id, name, k_id);
	}
	

}
