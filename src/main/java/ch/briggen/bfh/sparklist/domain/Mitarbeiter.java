package ch.briggen.bfh.sparklist.domain;
import java.util.Date;


/**
 * Einzelner Eintrag in der Mitarbeiter mit eindeutiger ID, Name, Vorname, Geschlecht, Strasse, Hausnummer, PLZ, Ort, Land, Geburtsdatum
 * @author Sandrine Zuber
 *
 */
public class Mitarbeiter {
	
	private long id;
	private String name;
	private String vorname; 
	private String geschlecht; 
	private String strasse; 
	private String hausnummer; 
	private Integer plz; 
	private String ort;
	private String land;
	private Date geburtsdatum; 
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Mitarbeiter()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige ID
	 * @param name Name des Mitarbeiters
	 * @param vorname Vorname des Mitarbeiters
	 * @param geschlecht Geschlecht des Mitarbeiters
	 * @param strasse Strasse des Mitarbeiters
	 * @param hausnummer Hausnummer des Mitarbeiters
	 * @param plz PLZ des Mitarbeiters
	 * @param ort Wohnort des Mitarbeiters
	 * @param land Land des Mitarbeiters
	 * @param geburtsdatum Geburtsdatum des Mitarbeiters
	 */
	public Mitarbeiter(long id, String name, String vorname, String geschlecht, String strasse, String hausnummer, Integer plz, String ort, String land, Date geburtsdatum)
	{
		this.id = id;
		this.name = name;
		this.vorname = vorname; 
		this.geschlecht = geschlecht;
		this.strasse = strasse; 
		this.hausnummer = hausnummer; 
		this.plz = plz; 
		this.ort = ort; 
		this.land = land;
		this.geburtsdatum = geburtsdatum; 
		
	}

	
	public String getMA_Name() {
		return name;
	}

	public void setMA_Name(String name) {
		this.name = name;
	}

	public long getMA_ID() {
		return id;
	}
	
	public void setMA_ID(long id) {
		this.id = id;
	}
	
	public String getMA_Vorname() {
		return vorname;
	}

	public void setMA_Vorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getMA_Geschlecht() {
		return geschlecht;
	}

	public void setMA_Geschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
	}
	
	public String getMA_Strasse() {
		return strasse;
	}

	public void setMA_Strasse(String strasse) {
		this.strasse = strasse;
	}
	
	public String getMA_Hausnummer() {
		return hausnummer;
	}

	public void setMA_Hausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}
	
	public Integer getMA_PLZ() {
		return plz;
	}

	public void setMA_PLZ(int plz) {
		this.plz = plz;
	}
	
	public String getMA_Ort() {
		return ort;
	}

	public void setMA_Ort(String ort) {
		this.ort = ort;
	}
	
	public String getMA_Land() {
		return land;
	}

	public void setMA_Land(String land) {
		this.land = land;
	}
	
	public Date getMA_Geburtsdatum() {
		return geburtsdatum;
	}

	public void setMA_Geburtsdatum(Date geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	
	
	@Override
	public String toString() {

		return "Mitarbeiter:{id: " + id + "; name: " + name + "; vorname: " + vorname + "; geschlecht: " + geschlecht + "; strasse: " + strasse + "; plz: " + plz + "; ort: " + ort + "; land: " + land + "; geburtsdatum: " + geburtsdatum + "}";
	}
	

}
