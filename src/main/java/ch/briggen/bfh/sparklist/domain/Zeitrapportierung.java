package ch.briggen.bfh.sparklist.domain;
import java.util.Date;


/**
 * Einzelner Eintrag in der Zeitrapportierung mit eindeutiger ID, Datum, Anzahl Stunden, Mitarbeiter-ID und Rapportierungsobjekt-ID
 * @author Dominique Gilgen
 *
 */
public class Zeitrapportierung {
	
	private long id;
	private Date tag;
	private float stunden;
	private int ma_id;
	private int ro_id; 
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Zeitrapportierung()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige ID
	 * @param tag Tag der Zeitrapportierung
	 * @param stunden Anzahl Stunden
	 * @param ma_id Mitarbeiter ID
	 * @param ro_id Rapportierungsobjekt ID
	 */
	public Zeitrapportierung(long id, Date tag, float stunden, int ma_id, int ro_id)
	{
		this.id = id;
		this.tag = tag;
		this.stunden = stunden; 
		this.ma_id = ma_id;
		this.ro_id = ro_id; 
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTag() {
		return tag;
	}

	public void setTag(Date tag) {
		this.tag = tag;
	}

	public float getStunden() {
		return stunden;
	}

	public void setStunden(float stunden) {
		this.stunden = stunden;
	}

	public int getMa_id() {
		return ma_id;
	}

	public void setMa_id(int ma_id) {
		this.ma_id = ma_id;
	}

	public int getRo_id() {
		return ro_id;
	}

	public void setRo_id(int ro_id) {
		this.ro_id = ro_id;
	}

	
	@Override
	public String toString() {

		return "Zeitrapportierung:{id: " + id + "; Tag: " + tag + "; Stunden: " + stunden + "; Mitarbeiter-ID: " + ma_id + "; Rapportierungsobjekt-ID: " + ro_id  + "}";
	}
	

}
