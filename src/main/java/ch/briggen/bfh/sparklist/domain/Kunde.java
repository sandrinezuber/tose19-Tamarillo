package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag eines Kundens mit Name und eindeutiger ID
 * @author Dominique Gilgen
 *
 */
public class Kunde {
	
	private long id;
	private String name;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Kunde()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige ID
	 * @param name Name des Kundens
	 */
	public Kunde(long id, String name)
	{
		this.id = id;
		this.name = name;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Kunde:{id: %d; name: %s;}", id, name);
	}
	

}
