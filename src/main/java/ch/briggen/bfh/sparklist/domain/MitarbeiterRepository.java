package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Mitarbeiter. 
 * Hier werden alle Funktionen für die DB-Operationen zu den Mitarbeitern implementiert
 * @author Sandrine Zuber
 **/


public class MitarbeiterRepository {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterRepository.class);
	

	/**
	 * Liefert alle Mitarbeiter in der Datenbank
	 * @return Collection aller Mitarbeiter
	 */
	public Collection<Mitarbeiter> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select ma_id, ma_name, ma_vorname, ma_geschlecht, ma_strasse, ma_hausnummer, ma_plz, ma_ort, ma_land, ma_geburtsdatum from mitarbeiter");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projects.";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Mitarbeiter mit dem angegebenen Namen
	 * @param ma_name
	 * @return Collection mit dem Namen "ma_name"
	 */
	public Collection<Mitarbeiter> getByName(String ma_name) {
		log.trace("getByName " + ma_name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select ma_id, ma_name, ma_vorname, ma_geschlecht, ma_strasse, ma_hausnummer, ma_plz, ma_ort, ma_land, ma_geburtsdatum from Projekt where ma_name=?");
			stmt.setString(1, ma_name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by name " + ma_name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert den Mitarbeiter mit der übergebenen ID
	 * @param ma_id des Mitarbeiters
	 * @return Mitarbeiter oder NULL
	 */
	public Mitarbeiter getById(long ma_id) {
		log.trace("getById " + ma_id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select ma_id, ma_name, ma_vorname, ma_geschlecht, ma_strasse, ma_hausnummer, ma_plz, ma_ort, ma_land, ma_geburtsdatum from Mitarbeiter where ma_id=?");
			stmt.setLong(1, ma_id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by id " + ma_id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert den übergebenen Mitarbeiter in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Mitarbeiter i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Mitarbeiter set ma_name=?, ma_vorname=?, ma_geschlecht=?, ma_strasse=?, ma_hausnummer=?, ma_plz=?, ma_ort=?, ma_land=?, ma_geburtsdatum=? where ma_id=?");
			stmt.setString(1, i.getMA_Name());
			stmt.setString(2, i.getMA_Vorname());
			stmt.setString(3, i.getMA_Geschlecht());
			stmt.setString(4, i.getMA_Strasse());
			stmt.setString(5, i.getMA_Hausnummer());
			stmt.setInt(6, i.getMA_PLZ());
			stmt.setString(7, i.getMA_Ort());
			stmt.setString(8, i.getMA_Land());
			stmt.setDate(9, new java.sql.Date(i.getMA_Geburtsdatum().getTime()));
			stmt.setLong(10, i.getMA_ID());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht den Mitarbeiter mit der angegebenen ID von der DB
	 * @param ma_id Mitarbeiter ID
	 */
	public void delete(long ma_id) {
		log.trace("delete " + ma_id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Mitarbeiter where ma_id=?");
			stmt.setLong(1, ma_id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projects by id " + ma_id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert den angegebenen Mitarbeiter in der DB. INSERT.
	 * @param mitarbeiterDetail zum nei erstellen eines Mitarbeiter
	 * @return Liefert die von der DB generierte id des neuen Mitarbeiter zurück
	 */
	public long insert(Mitarbeiter mitarbeiterDetail) {
		
		log.trace("insert " + mitarbeiterDetail);

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Mitarbeiter (ma_name, ma_vorname, ma_geschlecht, ma_strasse, ma_hausnummer, ma_plz, ma_ort, ma_land, ma_geburtsdatum) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, mitarbeiterDetail.getMA_Name());
			stmt.setString(2, mitarbeiterDetail.getMA_Vorname());
			stmt.setString(3, mitarbeiterDetail.getMA_Geschlecht());
			stmt.setString(4, mitarbeiterDetail.getMA_Strasse());
			stmt.setString(5, mitarbeiterDetail.getMA_Hausnummer());
			stmt.setInt(6, mitarbeiterDetail.getMA_PLZ());
			stmt.setString(7, mitarbeiterDetail.getMA_Ort());
			stmt.setString(8, mitarbeiterDetail.getMA_Land());
			stmt.setDate(9, new java.sql.Date(mitarbeiterDetail.getMA_Geburtsdatum().getTime()));
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + mitarbeiterDetail;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Mitarbeiter-Objekte. Siehe getByXXX Methoden.
	 * @author Sandrine Zuber
	 * @throws SQLException 
	 *
	 */
	private static Collection<Mitarbeiter> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Mitarbeiter> mitarbeiter = new LinkedList<Mitarbeiter>();
		while(rs.next())
		{
			Mitarbeiter i = new Mitarbeiter(rs.getLong("ma_id"),rs.getString("ma_name"), rs.getString("ma_vorname"), rs.getString("ma_geschlecht"), rs.getString("ma_strasse"), rs.getString("ma_hausnummer"), rs.getInt("ma_plz"), rs.getString("ma_ort"), rs.getString("ma_land"), rs.getDate("ma_geburtsdatum"));
			mitarbeiter.add(i);
		}
		return mitarbeiter;
	}


}
