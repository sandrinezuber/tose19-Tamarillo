package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Rapportierungsobjekte. 
 * Hier werden alle Funktionen für die DB-Operationen zu Rapportierungsobjekt implementiert
 * @author Dominique Gilgen
 *
 */


public class RapObjectRepository {
	
	private final Logger log = LoggerFactory.getLogger(RapObjectRepository.class);
	

	/**
	 * Liefert alle Rapportierungsobjekte in der Datenbank
	 * @return Collection aller Rapportierungsobjekte
	 */
	public Collection<RapObject> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select RO_ID, RO_Name, P_ID from Rapportierungsobjekt");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Rapportierungsobjekte. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Rapportierungsobjekte mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<RapObject> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select RO_ID, RO_Name, P_ID from Rapportierungsobjekt where RO_Name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Rapportierungsobjekte by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Rapportierungsobjekt mit der übergebenen ID
	 * @param id id des Rapportierungsobjekt
	 * @return Rapportierungsobjekt oder NULL
	 */
	public RapObject getById(long id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select RO_ID, RO_Name, P_ID from Rapportierungsobjekt where RO_ID=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Rapportierungsobjekte by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Rapportierungsobjekt in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(RapObject i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Rapportierungsobjekt set RO_Name=?, P_ID=? where RO_ID=?");
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getProjectID());
			stmt.setLong(3, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Rapportierungsobjekt " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Rapportierungsobjekt mit der angegebenen ID von der DB
	 * @param id Rapportierungsobjekt ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Rapportierungsobjekt where RO_ID=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Rapportierungsobjekte by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Rapportierungsobjekt in der DB. INSERT.
	 * @param i neu zu erstellendes Rapportierungsobjekt
	 * @return Liefert die von der DB generierte id des neuen Rapportierungsobjekt zurück
	 */
	public long insert(RapObject i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Rapportierungsobjekt (RO_Name, P_ID) values (?,?)");
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getProjectID());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Rapportierungsobjekt " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Rapportierungs-Objekte. Siehe getByXXX Methoden.
	 * @author Dominique Gilgen
	 * @throws SQLException 
	 *
	 */
	private static Collection<RapObject> mapItems(ResultSet rs) throws SQLException 
	{
		Collection<RapObject> list = new LinkedList<RapObject>();
		while(rs.next())
		{
			RapObject i = new RapObject(rs.getLong("ro_id"),rs.getString("ro_name"),rs.getInt("p_id"));
			list.add(i);
		}
		return list;
	}

}
