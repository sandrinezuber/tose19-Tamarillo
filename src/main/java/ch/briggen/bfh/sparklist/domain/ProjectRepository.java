package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projekte. 
 * Hier werden alle Funktionen für die DB-Operationen zu Project implementiert
 * @author Dominique Gilgen
 **/


public class ProjectRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRepository.class);
	

	/**
	 * Liefert alle Projekte in der Datenbank
	 * @return Collection aller Projects
	 */
	public Collection<Project> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select p_id, p_name, k_id from projekt");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projects.";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Projekte mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Project> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select p_id, p_name, k_id from Projekt where p_name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Projekt mit der übergebenen ID
	 * @param id des Projekts
	 * @return Projekt oder NULL
	 */
	public Project getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select p_id, p_name, k_id from Projekt where p_id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Projekt in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Project i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Projekt set p_name=?, k_id=? where p_id=?");
			stmt.setString(1, i.getName());
			stmt.setInt(2, i.getK_Id());
			stmt.setLong(3, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Projekt mit der angegebenen ID von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Projekt where p_id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projects by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Projekt in der DB. INSERT.
	 * @param projectDetail neu zu erstellendes Project
	 * @return Liefert die von der DB generierte id des neuen Projekt zurück
	 */
	public long insert(Project projectDetail) {
		
		log.trace("insert " + projectDetail);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Projekt (p_name, k_id) values (?,?)");
			stmt.setString(1, projectDetail.getName());
			stmt.setInt(2, projectDetail.getK_Id());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + projectDetail;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Project-Objekte. Siehe getByXXX Methoden.
	 * @author Dominique Gilgen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Project> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Project> project = new LinkedList<Project>();
		while(rs.next())
		{
			Project i = new Project(rs.getLong("p_id"),rs.getString("p_name"),rs.getInt("k_id"));
			project.add(i);
		}
		return project;
	}


}
