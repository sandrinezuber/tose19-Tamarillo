package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Kunden. 
 * Hier werden alle Funktionen für die DB-Operationen zu Kunden implementiert
 * @author Dominique Gilgen
 **/


public class KundeRepository {
	
	private final Logger log = LoggerFactory.getLogger(KundeRepository.class);
	

	/**
	 * Liefert alle Kunden in der Datenbank
	 * @return Collection aller Kunden
	 */
	public Collection<Kunde> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select k_id, k_name from kunde");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Kunden.";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Kunden mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Kunde> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select k_id, k_name from Kunde where k_name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Kunde by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert den Kunden mit der übergebenen ID
	 * @param ID des Kunden
	 * @return Kunden oder NULL
	 */
	public Kunde getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select k_id, k_name from Kunde where k_id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Kunden by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert den übergebene Kunden in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Kunde i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Kunde set k_name=? where k_id=?");
			stmt.setString(1, i.getName());
			stmt.setLong(2, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Kunde " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht den Kunden mit der angegebenen ID von der DB
	 * @param id Kuden ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Kunde where k_id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Kunde by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert den angegebene Kunden in der DB. INSERT.
	 * @param kundenDetail neu zu erstellender Kunde
	 * @return Liefert die von der DB generierte ID des neuen Kunden zurück
	 */
	public long insert(Kunde kundeDetail) {
		
		log.trace("insert " + kundeDetail);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Kunde (k_name) values (?)");
			stmt.setString(1, kundeDetail.getName());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating project " + kundeDetail;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Kunden-Objekte. Siehe getByXXX Methoden.
	 * @author Dominique Gilgen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Kunde> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Kunde> kunde = new LinkedList<Kunde>();
		while(rs.next())
		{
			Kunde i = new Kunde(rs.getLong("k_id"),rs.getString("k_name"));
			kunde.add(i);
		}
		return kunde;
	}


}
