package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag eines Rapportierungsobjektes mit Name und einer eindeutigen ID
 * @author Dominique Gilgen
 *
 */
public class RapObject {
	
	private long ro_id;
	private String ro_name;
	private int p_id;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public RapObject()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param ro_id Eindeutige ID
	 * @param ro_name Name des Rapportierungsobejktes
	 * @param p_id ID des Projektes
	 */
	public RapObject(long ro_id, String ro_name, int p_id)
	{
		this.ro_id = ro_id;
		this.ro_name = ro_name;
		this.p_id = p_id;
	}

	
	public String getName() {
		return ro_name;
	}

	public void setName(String name) {
		this.ro_name = name;
	}

	public int getProjectID() {
		return p_id;
	}

	public void setProjectID(int p_id) {
		this.p_id = p_id;
	}

	public long getId() {
		return ro_id;
	}
	
	public void setId(long id) {
		this.ro_id = id;
	}
	
	@Override
	public String toString() {
		return String.format("Rapportierungsobjekt:{id: %d; name: %s; projectid: %d;}", ro_id, ro_name, p_id);
	}
	

}
