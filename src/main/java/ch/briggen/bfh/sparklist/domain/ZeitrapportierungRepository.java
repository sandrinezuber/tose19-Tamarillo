package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für die Zeitrapportierung. 
 * Hier werden alle Funktionen für die DB-Operationen zu der Zeitrapportierung implementiert
 * @author Dominique Gilgen
 **/


public class ZeitrapportierungRepository {
	
	private final Logger log = LoggerFactory.getLogger(ZeitrapportierungRepository.class);
	

	/**
	 * Liefert alle Zeitrapportierungseinträge in der Datenbank
	 * @return Collection aller Zeitrapportierungen
	 */
	public Collection<Zeitrapportierung> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select zr_id, zr_tag, zr_anzahl_std, ma_id, ro_id from Zeitrapportierung");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Zeitrapportierungen.";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert den Zeitrapportierungseintrag mit der übergebenen ID
	 * @param zr_id des Zeitrapportierungseintrages
	 * @return Zeitrapportierung oder NULL
	 */
	public Zeitrapportierung getById(long zr_id) {
		log.trace("getById " + zr_id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select zr_id, zr_tag, zr_anzahl_std, ma_id, ro_id from Zeitrapportierung where zr_id=?");
			stmt.setLong(1, zr_id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Zeitrapportierung by id " + zr_id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert die übergebene Zeitrapportierung in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Zeitrapportierung i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Zeitrapportierung set zr_tag=?, zr_anzahl_std=?, ma_id=?, ro_id=? where zr_id=?");
			stmt.setDate(1, new java.sql.Date(i.getTag().getTime()));
			stmt.setFloat(2, i.getStunden());
			stmt.setInt(3, i.getMa_id());
			stmt.setInt(4, i.getRo_id());
			stmt.setLong(5, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Zeitrapportierung " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Zeitrapportierung mit der angegebenen ID von der DB
	 * @param zr_id Zeitrapportierungs ID
	 */
	public void delete(long zr_id) {
		log.trace("delete " + zr_id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Zeitrapportierung where zr_id=?");
			stmt.setLong(1, zr_id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Zeitrapportierung by id " + zr_id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert die angegebene Zeitrapportierung in der DB. INSERT.
	 * @param zeitrapportierungDetail zum erstellen einer Zeitrapportierung
	 * @return Liefert die von der DB generierte id der neuen Zeitrapportierung zurück
	 */
	public long insert(Zeitrapportierung zeitrapportierungDetail) {
		
		log.trace("insert " + zeitrapportierungDetail);

		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Zeitrapportierung (zr_tag, zr_anzahl_std, ma_id, ro_id) values (?, ?, ?, ?)");
			stmt.setDate(1, new java.sql.Date(zeitrapportierungDetail.getTag().getTime()));
			stmt.setFloat(2, zeitrapportierungDetail.getStunden());
			stmt.setInt(3, zeitrapportierungDetail.getMa_id());
			stmt.setInt(4, zeitrapportierungDetail.getRo_id());
			
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Zeitrapportierung " + zeitrapportierungDetail;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Zeitrapportierungs-Objekte. Siehe getByXXX Methoden.
	 * @author Dominique Gilgen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Zeitrapportierung> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Zeitrapportierung> zeitrapportierung = new LinkedList<Zeitrapportierung>();
		while(rs.next())
		{
			Zeitrapportierung i = new Zeitrapportierung(rs.getLong("zr_id"),rs.getDate("zr_tag"), rs.getFloat("zr_anzahl_std"), rs.getInt("ma_id"), rs.getInt("ro_id"));
			zeitrapportierung.add(i);
		}
		return zeitrapportierung;
	}


}
