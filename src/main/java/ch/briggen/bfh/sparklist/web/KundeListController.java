package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kunde;
import ch.briggen.bfh.sparklist.domain.KundeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Kundenliste
 *
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 **/
public class KundeListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(KundeListController.class);

	KundeRepository kundeRepo = new KundeRepository();

	/**
	 *Liefert die Kunden als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Kunden werden geladen und die Collection dann für das Template unter dem namen "kunden" bereitgestellt
		//Das Template muss dann auch den Namen "kunde" verwenden.
		HashMap<String, Collection<Kunde>> model = new HashMap<String, Collection<Kunde>>();
		model.put("list", kundeRepo.getAll());
		return new ModelAndView(model, "kundenListTemplate");
	}
}
