package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.KundeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Kunden
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class KundeDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(KundeDeleteController.class);
		
	private KundeRepository kundeRepo = new KundeRepository();
	

	/**
	 * Löscht das Projekt mit der übergebenen id in der Datenbank
	 * /kunde/delete&id=987 löscht den Kunden mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /kunde/delete
	 * 
	 * @return Redirect zurück zur Projektübersicht
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /kunde/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		kundeRepo.delete(longId);
		response.redirect("/kunde");
		return null;
	}
}


