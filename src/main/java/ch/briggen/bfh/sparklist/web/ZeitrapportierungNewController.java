package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Zeitrapportierung;
import ch.briggen.bfh.sparklist.domain.ZeitrapportierungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Mitarbeitern
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class ZeitrapportierungNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ZeitrapportierungNewController.class);
		
	private ZeitrapportierungRepository zeitrapportierungRepo = new ZeitrapportierungRepository();
	
	/**
	 * Erstellt eine neue Zeitrapportierung in der DB. Die ID wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /zeitrapportierung&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /zeitrapportierung/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Zeitrapportierung zeitrapportierungDetail = ZeitrapportierungWebHelper.zeitrapportierungFromWeb(request);
		log.trace("POST /zeitrapportierung/new mit zeitrapportierungDetail " + zeitrapportierungDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = zeitrapportierungRepo.insert(zeitrapportierungDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /zeitrapportierung
		response.redirect("/zeitrapportierung");
		return null;
	}
}


