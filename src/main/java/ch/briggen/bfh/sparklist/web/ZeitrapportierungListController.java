package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import ch.briggen.bfh.sparklist.domain.ZeitrapportierungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Zeitrapportierungsliste
 *
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 **/
public class ZeitrapportierungListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ZeitrapportierungListController.class);

	ZeitrapportierungRepository zeitrapportierungRepo = new ZeitrapportierungRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		MitarbeiterRepository mitarbeiter = new MitarbeiterRepository();
		RapObjectRepository rapobject = new RapObjectRepository();
		
		//Zeitrapportierungen werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", zeitrapportierungRepo.getAll());
		model.put("ma_list", mitarbeiter.getAll());
		model.put("rapobject_list", rapobject.getAll());
		
		return new ModelAndView(model, "zeitrapportierungListTemplate");
	}
}
