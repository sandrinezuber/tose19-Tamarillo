package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Mitarbeiterliste
 *
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/
public class MitarbeiterListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(MitarbeiterListController.class);

	MitarbeiterRepository repository = new MitarbeiterRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Mitarbeiter werden geladen und die Collection dann für das Template unter dem namen "mitarbeiter" bereitgestellt
		//Das Template muss dann auch den Namen "mitarbeiter" verwenden.
		HashMap<String, Collection<Mitarbeiter>> model = new HashMap<String, Collection<Mitarbeiter>>();
		model.put("list", repository.getAll());
		return new ModelAndView(model, "mitarbeiterListTemplate");
	}
}
