package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.RapObject;
import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Rapportierungsobjekte
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 *
 */

public class RapObjectEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(RapObjectEditController.class);
	
	
	
	private RapObjectRepository RO_Repo = new RapObjectRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Rapportierungsobjekt. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Rapportierungsobjekt erstellt (Aufruf von /rapobject/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Rapportierungsobjekt mit der übergebenen id upgedated (Aufruf /rapoject/save)
	 * Hört auf GET /rapobject
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "rapobjectDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		ProjectRepository project = new ProjectRepository();
		Collection<Project> collection = project.getAll();		
		model.put("ProjectList", collection);

		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /rapobject für INSERT mit id " + idString);
			//der Submit-Button ruft /rapobject/new auf --> INSERT
			model.put("postAction", "/rapobject/new");
			model.put("RapObjectDetail", new RapObject());

		}
		else
		{
			log.trace("GET /rapobject für UPDATE mit id " + idString);
			//der Submit-Button ruft /rapobject/update auf --> UPDATE
			model.put("postAction", "/rapobject/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "rapobjectDetail" hinzugefügt. rapobjectDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			RapObject i = RO_Repo.getById(id);
			model.put("RapObjectDetail", i);
		}
		
		//das Template rapobjectDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "rapObjectDetailTemplate");
	}
	
	
	
}


