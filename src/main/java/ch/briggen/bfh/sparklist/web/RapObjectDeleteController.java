package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Rapportierungsobjekte
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 *
 */

public class RapObjectDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(RapObjectDeleteController.class);
		
	private RapObjectRepository RO_Repo = new RapObjectRepository();
	

	/**
	 * Löscht das Rapportierungsobjekt mit der übergebenen id in der Datenbank
	 * /rapobject/delete&id=987 löscht das Rapportierungsobjekt mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /rapoject/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /rapobject/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		RO_Repo.delete(longId);
		response.redirect("/rapobject");
		return null;
	}
}


