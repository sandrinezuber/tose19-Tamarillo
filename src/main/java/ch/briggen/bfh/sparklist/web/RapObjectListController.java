package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Rapportierungsobjektliste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Dominique Gilgen
 *
 */
public class RapObjectListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(RapObjectListController.class);

	private RapObjectRepository RO_Repo = new RapObjectRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		ProjectRepository prepo = new ProjectRepository();
		
		//Rapportierungsobejkte werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", RO_Repo.getAll());
		model.put("ProjectList", prepo.getAll());
		return new ModelAndView(model, "rapObjectListTemplate");
	}
}
