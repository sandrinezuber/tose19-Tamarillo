package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kunde;
import spark.Request;

class KundeWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(KundeWebHelper.class);
	
	public static Kunde projectFromWeb(Request request)
	{
		return new Kunde(
				Long.parseLong(request.queryParams("kundeDetail.id")),
				request.queryParams("kundeDetail.name"));
	}

}
