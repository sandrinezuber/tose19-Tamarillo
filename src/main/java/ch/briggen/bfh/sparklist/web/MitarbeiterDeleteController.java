package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Mitarbeiter
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class MitarbeiterDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterDeleteController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	

	/**
	 * Löscht den Mitarbeiter mit der übergebenen ID in der Datenbank
	 * /mitarbeiter/delete&id=987 löscht den Mitarbeiter aus der Datenbank
	 * 
	 * Hört auf GET /mitarbeiter/delete
	 * 
	 * @return Redirect zurück zur Mitarbeiterübersicht
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /mitarbeiter/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		mitarbeiterRepo.delete(longId);
		response.redirect("/mitarbeiter");
		return null;
	}
}


