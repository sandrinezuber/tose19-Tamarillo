package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import ch.briggen.bfh.sparklist.domain.Zeitrapportierung;
import ch.briggen.bfh.sparklist.domain.ZeitrapportierungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Zeitrapportierungen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
**/

public class ZeitrapportierungEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ZeitrapportierungEditController.class);
	
	
	
	private ZeitrapportierungRepository zeitrapportierungRepo = new ZeitrapportierungRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten einer Zeitrapportierung. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars eine neue Zeitrapportierung erstellt (Aufruf von /zeitrapportierung/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars die Zeitrapportierung mit der übergebenen id upgedated (Aufruf /zeitrapportierung/save)
	 * Hört auf GET /zeitrapportierung
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "zeitrapportierungDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		MitarbeiterRepository mitarbeiter = new MitarbeiterRepository();
		RapObjectRepository rapobject = new RapObjectRepository();
		
		model.put("ma_list", mitarbeiter.getAll());
		model.put("rapobject_list", rapobject.getAll());
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /zeitrapportierung für INSERT mit id " + idString);
			//der Submit-Button ruft /zeitrapportierung/new auf --> INSERT
			model.put("postAction", "/zeitrapportierung/new");
			model.put("zeitrapportierungDetail", new Zeitrapportierung());

		}
		else
		{
			log.trace("GET /zeitrapportierung für UPDATE mit id " + idString);
			//der Submit-Button ruft /zeitrapportierung/update auf --> UPDATE
			model.put("postAction", "/zeitrapportierung/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "zeitrapportierungDetail" hinzugefügt. zeitrapportierungDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Zeitrapportierung i = zeitrapportierungRepo.getById(id);
			model.put("zeitrapportierungDetail", i);
		}
		
		//das Template mitarbeiterDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "zeitrapportierungDetailTemplate");
	}
	
	
	
}


