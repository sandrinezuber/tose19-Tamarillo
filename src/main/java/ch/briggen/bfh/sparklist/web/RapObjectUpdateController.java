package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RapObject;
import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Rapportierungsobjekte
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 *
 */

public class RapObjectUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(RapObjectUpdateController.class);
		
	private RapObjectRepository RO_Repo = new RapObjectRepository();
	


	/**
	 * Schreibt das geänderte Rapportierungsobjekt zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/rapobject) mit der Rapportierungsobjekt-ID als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /rapobject/update
	 * 
	 * @return redirect nach /rapobject: via Browser wird /rapobject aufgerufen, also editrapobject weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		RapObject RapObjectDetail = RapObjectWebHelper.RapObjectFromWeb(request);
		
		log.trace("POST /rapobject/update mit itemDetail " + RapObjectDetail);
		
		//Speichern des Rapportierungsobjekt in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /rapobject&id=3 (wenn rapobjectDetail.getId == 3 war)
		RO_Repo.save(RapObjectDetail);
		response.redirect("/rapobject");
		return null;
	}
}


