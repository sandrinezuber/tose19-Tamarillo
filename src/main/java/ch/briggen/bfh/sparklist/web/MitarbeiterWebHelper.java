package ch.briggen.bfh.sparklist.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import spark.Request;

class MitarbeiterWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MitarbeiterWebHelper.class);
	
	public static Mitarbeiter mitarbeiterFromWeb(Request request) throws ParseException
	{
		String dateFormatPattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatPattern);
		Date date = simpleDateFormat.parse(request.queryParams("mitarbeiterDetail.ma_geburtsdatum"));

		return new Mitarbeiter(
				Long.parseLong(request.queryParams("mitarbeiterDetail.ma_id")),
				request.queryParams("mitarbeiterDetail.ma_name"),
				request.queryParams("mitarbeiterDetail.ma_vorname"),
				request.queryParams("mitarbeiterDetail.ma_geschlecht"),
				request.queryParams("mitarbeiterDetail.ma_strasse"),
				request.queryParams("mitarbeiterDetail.ma_hausnummer"),		
				Integer.parseInt(request.queryParams("mitarbeiterDetail.ma_plz")),
				request.queryParams("mitarbeiterDetail.ma_ort"),
				request.queryParams("mitarbeiterDetail.ma_land"),
				date);	
	}

}
