package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kunde;
import ch.briggen.bfh.sparklist.domain.KundeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Kunden
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class KundeUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(KundeUpdateController.class);
		
	private KundeRepository kundeRepo = new KundeRepository();
	


	/**
	 * Schreibt den geänderte Kunden zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/kunde) mit der Kunden-ID als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /kunde/update
	 * 
	 * @return redirect nach /kunde: via Browser wird /kunde aufgerufen, also editKunde weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Kunde kundeDetail = KundeWebHelper.projectFromWeb(request);
		
		log.trace("POST /kunde/update mit kundeDetail " + kundeDetail);
		
		//Speichern den Kunden in den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /kunde&id=3 (wenn kundeDetail.getId == 3 war)
		kundeRepo.save(kundeDetail);
		response.redirect("/kunde");
		return null;
	}
}


