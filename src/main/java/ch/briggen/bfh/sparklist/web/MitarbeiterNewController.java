package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Mitarbeitern
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class MitarbeiterNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterNewController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	
	/**
	 * Erstellt einen neuen Mitarbeiter in der DB. Die ID wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /mitarbeiter&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /mitarbeiter/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter mitarbeiterDetail = MitarbeiterWebHelper.mitarbeiterFromWeb(request);
		log.trace("POST /mitarbeiter/new mit mitarbeiterDetail " + mitarbeiterDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = mitarbeiterRepo.insert(mitarbeiterDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /mitarbeiter?id=432932
		response.redirect("/mitarbeiter");
		return null;
	}
}


