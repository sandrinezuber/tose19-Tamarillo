package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kunde;
import ch.briggen.bfh.sparklist.domain.KundeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Kunden
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class KundeNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(KundeNewController.class);
		
	private KundeRepository kundeRepo = new KundeRepository();
	
	/**
	 * Erstellt einen neuen Kunden in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /kunde&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /kunde/new
	 * 
	 * @return Redirect zurück zur Kundenmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Kunde kundeDetail = KundeWebHelper.projectFromWeb(request);
		log.trace("POST /kunde/new mit kundeDetail " + kundeDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = kundeRepo.insert(kundeDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /kunde?id=432932
		response.redirect("/kunde");
		return null;
	}
}


