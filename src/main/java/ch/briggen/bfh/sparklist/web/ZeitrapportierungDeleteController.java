package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ZeitrapportierungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelne Zeitrapportierungen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 **/

public class ZeitrapportierungDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ZeitrapportierungDeleteController.class);
		
	private ZeitrapportierungRepository zeitrapportierungRepo = new ZeitrapportierungRepository();
	

	/**
	 * Löscht die Zeitrapportierung mit der übergebenen ID in der Datenbank
	 * /zeitrapportierung/delete&id=987 löscht die Zeitrapportierung aus der Datenbank
	 * 
	 * Hört auf GET /zeitrapportierung/delete
	 * 
	 * @return Redirect zurück zur Zeitrapportierungsübersicht
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /zeitrapportierung/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		zeitrapportierungRepo.delete(longId);
		response.redirect("/zeitrapportierung");
		return null;
	}
}


