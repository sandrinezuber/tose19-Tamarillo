package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Mitarbeiter
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
 **/

public class MitarbeiterUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterUpdateController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	


	/**
	 * Schreibt den geänderten Mitrabeiter zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/mitarbeiter) mit der Mitarbeiter-ID als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /mitarbeiter/update
	 * 
	 * @return redirect nach /mitarbeiter: via Browser wird /mitarbeiter aufgerufen, also editMitarbeit weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter mitarbeiterDetail = MitarbeiterWebHelper.mitarbeiterFromWeb(request);
		
		log.trace("POST /mitarbeiter/update mit mitarbeiterDetail " + mitarbeiterDetail);
		
		//Speichern den Mitarbeiter in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /mitarbeiter&id=3 (wenn mitarbeiterDetail.getId == 3 war)
		mitarbeiterRepo.save(mitarbeiterDetail);
		response.redirect("/mitarbeiter");
		return null;
	}
}


