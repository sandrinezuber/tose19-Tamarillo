package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RapObject;
import spark.Request;

class RapObjectWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(RapObjectWebHelper.class);
	
	public static RapObject RapObjectFromWeb(Request request)
	{
		return new RapObject(
				Long.parseLong(request.queryParams("RapObjectDetail.ro_id")),
				request.queryParams("RapObjectDetail.ro_name"),
				Integer.parseInt(request.queryParams("RapObjectDetail.p_id")));
	}

}
