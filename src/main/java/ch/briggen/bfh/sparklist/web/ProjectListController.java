package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.KundeRepository;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Projektliste
 *
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 **/
public class ProjectListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjectListController.class);

	ProjectRepository repository = new ProjectRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		KundeRepository kunde = new KundeRepository();
		
		//Projekte werden geladen und die Collection dann für das Template unter dem namen "project" bereitgestellt
		//Das Template muss dann auch den Namen "project" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", repository.getAll());
		model.put("KundenList", kunde.getAll());
		return new ModelAndView(model, "projectListTemplate");
	}
}
