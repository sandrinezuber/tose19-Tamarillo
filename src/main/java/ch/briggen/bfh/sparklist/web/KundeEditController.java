package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Kunde;
import ch.briggen.bfh.sparklist.domain.KundeRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Kunden
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Sandrine Zuber
**/

public class KundeEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(KundeEditController.class);
	
	
	
	private KundeRepository kundeRepo = new KundeRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Kunden. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer Kunde erstellt (Aufruf von /kunde/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars der Kunde mit der übergebenen id upgedated (Aufruf /kunde/save)
	 * Hört auf GET /kunde
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "kundeDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /kunde für INSERT mit id " + idString);
			//der Submit-Button ruft /kunde/new auf --> INSERT
			model.put("postAction", "/kunde/new");
			model.put("kundeDetail", new Kunde());

		}
		else
		{
			log.trace("GET /kunde für UPDATE mit id " + idString);
			//der Submit-Button ruft /kunde/update auf --> UPDATE
			model.put("postAction", "/kunde/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "kundeDetail" hinzugefügt. kundeDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Kunde i = kundeRepo.getById(id);
			model.put("kundeDetail", i);
		}
		
		//das Template kundeDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "kundenDetailTemplate");
	}
	
	
	
}


