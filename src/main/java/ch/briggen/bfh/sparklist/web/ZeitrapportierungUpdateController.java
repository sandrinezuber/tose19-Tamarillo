package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Zeitrapportierung;
import ch.briggen.bfh.sparklist.domain.ZeitrapportierungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Zeitrapportierungen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 **/

public class ZeitrapportierungUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ZeitrapportierungUpdateController.class);
		
	private ZeitrapportierungRepository zeitrapportierungRepo = new ZeitrapportierungRepository();
	


	/**
	 * Schreibt die geänderte Zeitrapportierung zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Übersichtsseite (/zeitrapportierunge)
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /zeitrapportierung/update
	 * 
	 * @return redirect nach /zeitrapportierung: via Browser wird /zeitrapportierung aufgerufen, also editZeitrapportierung weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Zeitrapportierung zeitrapportierungDetail = ZeitrapportierungWebHelper.zeitrapportierungFromWeb(request);
		
		log.trace("POST /zeitrapportierung/update mit zeitrapportierungDetail " + zeitrapportierungDetail);
		
		//Speichern die Zeitrapportierung in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /zeitrapportierung
		zeitrapportierungRepo.save(zeitrapportierungDetail);
		response.redirect("/zeitrapportierung");
		return null;
	}
}


