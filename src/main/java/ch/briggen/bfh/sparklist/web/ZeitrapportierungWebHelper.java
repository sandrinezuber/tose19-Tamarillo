package ch.briggen.bfh.sparklist.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Zeitrapportierung;
import spark.Request;

class ZeitrapportierungWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ZeitrapportierungWebHelper.class);
	
	public static Zeitrapportierung zeitrapportierungFromWeb(Request request) throws ParseException
	{
		String dateFormatPattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormatPattern);
		Date date = simpleDateFormat.parse(request.queryParams("zeitrapportierungDetail.tag"));

		return new Zeitrapportierung(
				Long.parseLong(request.queryParams("zeitrapportierungDetail.id")),
				date,
				Float.parseFloat(request.queryParams("zeitrapportierungDetail.stunden")),
				Integer.parseInt(request.queryParams("zeitrapportierungDetail.ma_id")),	
				Integer.parseInt(request.queryParams("zeitrapportierungDetail.ro_id")));	
	}

}
