package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RapObject;
import ch.briggen.bfh.sparklist.domain.RapObjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Rapportierungsobjekte
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominique Gilgen
 *
 */

public class RapObjectNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(RapObjectNewController.class);
		
	private RapObjectRepository RO_Repo = new RapObjectRepository();
	
	/**
	 * Erstellt ein neues Rapportierungsobjekt in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /rapobject&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /rapobject/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		RapObject RapObjectDetail = RapObjectWebHelper.RapObjectFromWeb(request);
		log.trace("POST /rapobject/new mit itemDetail " + RapObjectDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = RO_Repo.insert(RapObjectDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /rapobject?id=432932
		response.redirect("/rapobject");
		return null;
	}
}


