package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.HomepageController;
import ch.briggen.bfh.sparklist.web.KundeDeleteController;
import ch.briggen.bfh.sparklist.web.KundeEditController;
import ch.briggen.bfh.sparklist.web.KundeListController;
import ch.briggen.bfh.sparklist.web.KundeNewController;
import ch.briggen.bfh.sparklist.web.KundeUpdateController;
import ch.briggen.bfh.sparklist.web.MitarbeiterDeleteController;
import ch.briggen.bfh.sparklist.web.MitarbeiterEditController;
import ch.briggen.bfh.sparklist.web.MitarbeiterListController;
import ch.briggen.bfh.sparklist.web.MitarbeiterNewController;
import ch.briggen.bfh.sparklist.web.MitarbeiterUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectEditController;
import ch.briggen.bfh.sparklist.web.ProjectListController;
import ch.briggen.bfh.sparklist.web.ProjectNewController;
import ch.briggen.bfh.sparklist.web.ProjectUpdateController;
import ch.briggen.bfh.sparklist.web.RapObjectDeleteController;
import ch.briggen.bfh.sparklist.web.RapObjectEditController;
import ch.briggen.bfh.sparklist.web.RapObjectListController;
import ch.briggen.bfh.sparklist.web.RapObjectNewController;
import ch.briggen.bfh.sparklist.web.RapObjectUpdateController;
import ch.briggen.bfh.sparklist.web.ZeitrapportierungDeleteController;
import ch.briggen.bfh.sparklist.web.ZeitrapportierungEditController;
import ch.briggen.bfh.sparklist.web.ZeitrapportierungListController;
import ch.briggen.bfh.sparklist.web.ZeitrapportierungNewController;
import ch.briggen.bfh.sparklist.web.ZeitrapportierungUpdateController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
		
		get("/", new HomepageController(), new UTF8ThymeleafTemplateEngine());
		
		get("/project", new ProjectListController(), new UTF8ThymeleafTemplateEngine());
		get("/project/edit", new ProjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/project/delete", new ProjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/project/new", new ProjectNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/mitarbeiter", new MitarbeiterListController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/edit", new MitarbeiterEditController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/update", new MitarbeiterUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/delete", new MitarbeiterDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/new", new MitarbeiterNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/rapobject", new RapObjectListController(), new UTF8ThymeleafTemplateEngine());
		get("/rapobject/edit", new RapObjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/rapobject/update", new RapObjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/rapobject/delete", new RapObjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/rapobject/new", new RapObjectNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/kunde", new KundeListController(), new UTF8ThymeleafTemplateEngine());
		get("/kunde/edit", new KundeEditController(), new UTF8ThymeleafTemplateEngine());
		post("/kunde/update", new KundeUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/kunde/delete", new KundeDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/kunde/new", new KundeNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/zeitrapportierung", new ZeitrapportierungListController(), new UTF8ThymeleafTemplateEngine());
		get("/zeitrapportierung/edit", new ZeitrapportierungEditController(), new UTF8ThymeleafTemplateEngine());
		post("/zeitrapportierung/update", new ZeitrapportierungUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/zeitrapportierung/delete", new ZeitrapportierungDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/zeitrapportierung/new", new ZeitrapportierungNewController(), new UTF8ThymeleafTemplateEngine());

	}

}
