$(document).ready( function () {
    $('#sort').DataTable({
    	"paging": false,
    	"info": false,
    	"order": [[0, "asc"]],
    	"dom": '<"toolbar"f<"clear">>rt',
    	"language": {"search": "Suchen:"},
    	"columnDefs": [
    		{"orderable": false, "targets": [4, 5]}
    	]
    });
} );