insert into Kunde values
	(1, 'Architekturbüro Müller'), 
	(2, 'Coiffeur Haarpracht'), 
	(3, 'Tourismusbüro Ins');

insert into Projekt values
	(1, 'Projekt Mango', 1), 
	(2, 'Projekt Erdbeere', 2), 
	(3, 'Projekt Apfel', 3),
	(4, 'Projekt Birne', 1);

insert into Rapportierungsobjekt values
	(1, 'Offerte Mango', 1), 
	(2, 'Dokumentation Mango', 1), 
	(3, 'Offerte Erdbeere', 2),
	(4, 'Offerte Apfel', 3);

insert into Mitarbeiter values
	(1, 'Leuver', 'Lars', 'Männlich', 'Blumenstrasse', 12, 3012, 'Bern', 'CH', '1947-04-01'),
	(2, 'Rieser', 'Steven', 'Männlich', 'Dorfstrasse', '6a', 3816, 'Grindelwald', 'CH', '1968-10-15'),
	(3, 'Zimmer', 'Nicole', 'Weiblich', 'Ramusweg', 9, 3032, 'Hinterkappelen', 'CH', '1989-07-28'),
	(4, 'Schindler', 'Martina', 'Weiblich', 'Stadtbachstrasse', '89b', 3001, 'Bern', 'CH', '1997-02-16');

insert into Zeitrapportierung values
	(1, '2019-04-12', 2.1, 1, 2),
	(2, '2019-04-28', 1.6, 3, 2),
	(3, '2019-05-01', 3.6, 4, 4),
	(4, '2019-05-11', 1.1, 1, 1);