# Rapid-Rapp

## Intro

This is a simple application skeleton for teaching web applications in Java. We use:
-	Thymeleaf, HTML,  CSS as view technologies
- 	H2 SQL as database
-	And SparkJava as web framework